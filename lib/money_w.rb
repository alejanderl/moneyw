require "money_w/version"
require "money_w/contable"

module MoneyW

  class Money
    include Contable

    attr_accessor :amount, :currency

    def self.conversion_rates currency, rates
      @@conversion_rates ||= {}
      @@conversion_rates[currency] = rates
    end

    def initialize *args
      self.amount = args.first
      self.currency = args[1]
    end

    def convert_to conversion_currency
      if conversion_currency.to_s != 'EUR'
        convert_to_euros*@@conversion_rates['EUR'][conversion_currency.to_s].to_f
      else
        convert_to_euros
      end.round(2)
    end

    def inspect
      "#{amount.round(2)} #{currency}"
    end

    private

    def convert_to_euros
      # Easier to work having a common start point
      (self.currency == 'EUR' ? amount : self.amount/@@conversion_rates['EUR'][self.currency.to_s]).round(2)
    end

  end
end
