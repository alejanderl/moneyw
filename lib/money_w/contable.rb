module MoneyW
  module Contable

    def == quantity
      numerics '==', quantity
    end

    def method_missing(method_sym, *arguments, &block)
      if ['+','-','<','>','/', '*'].include? method_sym.to_s
        numerics method_sym, arguments[0]
      else
        super
      end
    end


    def numerics operator, value_to_operate
      numeric_value = value_to_operate.is_a?(Money) ? value_to_operate.convert_to(currency) : value_to_operate
      self.amount.send operator, numeric_value
    end

  end
end
