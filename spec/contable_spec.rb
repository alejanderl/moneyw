require 'spec_helper'

describe MoneyW::Contable do

  before(:each) do
    MoneyW::Money.conversion_rates('EUR', {
      'USD'     => 1.11,
      'Bitcoin' => 0.0047
      })
  end

  it "can + money amount instances" do
    fifty_eur = MoneyW::Money.new 50, 'EUR'
    twenty_dollars = MoneyW::Money.new(20, 'USD')
    expect(fifty_eur + twenty_dollars).to eq 68.02
  end

  it "can - money amount instances" do
    fifty_eur = MoneyW::Money.new 50, 'EUR'
    twenty_dollars = MoneyW::Money.new(20, 'USD')
    expect(fifty_eur - twenty_dollars).to eq 31.98
  end

  it "compare > money instances amount" do
    fifty_eur = MoneyW::Money.new 50, 'EUR'
    fifty_dollars = MoneyW::Money.new(50, 'USD')
    expect(fifty_eur > fifty_dollars).to be_truthy
  end

  it "compare < money instances amounts" do
    fifty_eur = MoneyW::Money.new 50, 'EUR'
    fifty_dollars = MoneyW::Money.new(50, 'USD')
    expect(fifty_eur < fifty_dollars).to be false
  end

  it "compare == money instances amounts" do
    fifty_eur = MoneyW::Money.new 50, 'EUR'
    expect(fifty_eur == MoneyW::Money.new(50, 'EUR')).to be true
  end

  it "operate money amount / with numbers" do
    fifty_eur = MoneyW::Money.new 50, 'EUR'
    expect(fifty_eur / 2).to eq 25
  end

  it 'operate money amount * with numbers' do
    twenty_dollars = MoneyW::Money.new(20, 'USD')
    expect(twenty_dollars * 3).to eq 60
  end

end