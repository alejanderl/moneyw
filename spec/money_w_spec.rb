require 'spec_helper'

describe MoneyW::Money do
  before(:each) do
    MoneyW::Money.conversion_rates('EUR', {
      'USD'     => 1.11,
      'Bitcoin' => 0.0047
      })
  end

  it 'has a version number' do
    expect(MoneyW::VERSION).not_to be nil
  end

  it '.conversion_rates' do
    expect(MoneyW::Money.class_variable_get '@@conversion_rates').not_to be_nil
  end

  it '#amount' do
    fifty_eur = MoneyW::Money.new 50, 'EUR'
    expect(fifty_eur.amount).to eq 50
  end

  it '#curreny' do
    fifty_eur = MoneyW::Money.new 50, 'EUR'
    expect(fifty_eur.currency).to eq "EUR"
  end

  it '#inspect' do
    fifty_eur = MoneyW::Money.new 50, 'EUR'
    expect(fifty_eur.inspect).to eq "50.0 EUR"
  end

  it '#convert_to' do
    fifty_eur = MoneyW::Money.new 50, 'EUR'
    expect(fifty_eur.convert_to :USD).to eq 55.5
    twenty_dollars = MoneyW::Money.new(20, 'USD')
    expect(twenty_dollars.convert_to :EUR).to eq 18.02
  end

end
